#include <iostream>
#include <vector>
#include <ios>

// POST: Gibt den zugehörigen Grossbuchstaben zurück
char char_to_upper(char letter) {
  // Nur für kleine Buchstaben (Sonderzeichen bleiben wie sie sind!!!)
  if ('a' <= letter && letter <= 'z') {
    letter -= 'a' - 'A';  // Differenz zwischen Gross- und Kleinbuchstaben bleibt konstant, da sie im ASCII Format in einer Reihe sind
  }
  return letter;
}

// POST: Converts all letters to upper-case.
std::vector<char> to_upper(std::vector<char> letters) {
  for (unsigned int i = 0; i < letters.size(); ++i) {
    letters[i] = char_to_upper(letters[i]);
  }
  return letters;
}

int main () {
  //Einstellung von std::cin
  std::cin >> std::noskipws;
  
  std::vector<char> letters;
  char ch;
  
  //Einlesen der Chars
  do {
    std::cin >> ch;
    letters.push_back(ch);
  } while (ch != '\n');
  
  letters = to_upper(letters);
  
  //Ausgabe der Uppercase-Letters
  for (unsigned int i = 0; i < letters.size(); ++i) {
    std::cout << letters[i];
  }
  return 0;
}
