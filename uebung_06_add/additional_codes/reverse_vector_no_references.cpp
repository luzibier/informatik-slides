#include <iostream>
#include <vector>
#include <cassert>

// PRE: length is the real length of the vector sequence.
// POST: Reverses the sequence stored in the vector sequence.
std::vector<int> reverse(std::vector<int> sequence, unsigned int length) {
  std::vector<int> reversed_vector (sequence.size());
  if (length >= 1) {
    int front = 0;
    int back = length-1;  // Be careful: with length == 0, we would get underflow!
    while (0 <= back) {
      reversed_vector.at(front) = sequence.at(back);
      ++front;
      --back;
    }
  }
  return reversed_vector;
}

int main () {
  unsigned int length;
  std::vector<int> sequence (10);
  
  // Step 1: Read input.
  std::cin >> length;
  if (length > 10) {
    std::cout << "Bad input" << std::endl;
    return 0;
  }
  for (unsigned int i = 0; i < length; ++i) {
    std::cin >> sequence.at(i);
  }
  
  // Step 2: Reverse sequence;
  sequence = reverse(sequence, length);
  
  // Step 3: Output
  for (unsigned int i = 0; i < length; ++i) {
    std::cout << sequence.at(i) << " ";
  }
  std::cout << std::endl;
  
  return 0;
}
