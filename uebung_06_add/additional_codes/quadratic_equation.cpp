#include <iostream>
#include <cassert>
#include <cmath>

// Löst quadratische Gleichungen (Nur den Realteil)
/*
  a, b, c Variablen aus der Mitternachtsformel
  x1, und x2 referenzierbare Variablen, in der die Lösungen gespeichert werden
  return value: Anzahl lösungen. -1 = unendlich viele Lsg
*/

int solve_quadratic_equation(double a, double b, double c, double& x1, double& x2) {
  double epsilon = 1.0/1024; // Toleranzwert für Vergleiche von doubles
  if (a == 0.0) {
    if (b == 0.0) {
      if (c == 0.0) {
        return -1;
      } else {
        return 0;
      }
    } else {
      x1 = -c/b;
      return 1;
    }
  } else {
    double discrimintant_square = b*b - 4*a*c;
    std::cerr << discrimintant_square << std::endl;
    if (discrimintant_square > epsilon) {
      double discrimintant = std::sqrt(discrimintant_square);
      x1 = (-b + discrimintant)/(2*a);
      x2 = (-b - discrimintant)/(2*a);
      return 2;
    } else if (std::abs(discrimintant_square) < epsilon) {
      x1 = -b/(2*a);
      return 1;
    } else {
      return 0;
    }
  }
}

int main () {
  double a, b, c;
  double x1 = 0.0;
  double x2 = 0.0;
  int solutions;
  
  std::cin >> a >> b >> c;
  
  solutions = solve_quadratic_equation(a, b, c, x1, x2);
  
  switch (solutions) {
    case -1:
      std::cout << "There are infinitely many solutions." << std::endl;
      break;
    case 0:
      std::cout << "There are no solutions." << std::endl;
      break;
    case 1:
      std::cout << "Solution x = " << x1 << std::endl;
      break;
    case 2:
      std::cout << "Solution x1 = " << x1 << " x2 = " << x2 << std::endl;
      break;
    default:
      assert(false);
  }
}
