#include <iostream>
#include <vector>
#include <cassert>

// POST: the targets of i and j got their values swapped
// Siehe Übungsfolien
void swap (int& i, int& j) {
    const int tmp = i;
    i = j;
    j = tmp;
}

// PRE: length is the real length of the vector sequence.
// POST: Reverses the sequence stored in the vector sequence.
// Vektor wird als Referenz übergeben, da so der Vektor selbst direkt beeinflusst wird. 
void reverse(std::vector<int>& sequence, unsigned int length) {
  if (length > 1) {
    int front = 0;
    int back = length-1;  // Be careful: with length == 0, we would get underflow!
    while (front < back) {
      swap(sequence.at(front), sequence.at(back));
      ++front;
      --back;
    }
  }
}

int main () {
  unsigned int length;
  std::vector<int> sequence (10);
  
  // Step 1: Read input.
  std::cin >> length;
  if (length > 10) {
    std::cout << "Bad input" << std::endl;
    return 0;
  }
  for (unsigned int i = 0; i < length; ++i) {
    std::cin >> sequence.at(i);
  }
  
  // Step 2: Reverse sequence;
  reverse(sequence, length);
  
  // Step 3: Output
  for (unsigned int i = 0; i < length; ++i) {
    std::cout << sequence.at(i) << " ";
  }
  std::cout << std::endl;
  
  return 0;
}
