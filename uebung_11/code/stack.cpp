#include <iostream>
#include "stack.h"

void Stack::push(std::string input){//Member funktion von Stack
  Node* new_node = new Node{input, this->top_node}; // Mache neuen Node irgendwo im Speicher
              // below ist der aktuell oberster Node
  this->top_node = new_node; // Neuer Node ist der oberste Node
}

std::string Stack::pop(){//Member funktion von Stack
  if (this->top_node == nullptr){ // Falls stack leer kann nicht pop aufgerufen werden
    std::cout << "WARNING: Node Empty" << std::endl;
    return "";
  }
  //Else
  Node* actual = this->top_node; // aktueller Node zeigt auf den Top_node
  std::string res = actual->data; //Speichere Resultat, dass der Node gelöscht werden kann.
  
  this->top_node = actual->below; // Setze den top_node auf den Node unter dem aktuellen Node
  delete actual; // Lösche den Node, wird icht mehr gebraucht.
  
  return res; // gib resultat aus
}

