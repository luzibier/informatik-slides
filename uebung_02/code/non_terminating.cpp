#include <iostream>
#include <cassert>
#define NDEBUG
int main () {
    const int n = 6;
    // Compute n^12
    int nenner = 0;
    assert(nenner != 0);
    int prod = 1;
    for (int i = 1; 1 <= i && i < 13; ++i) {
        prod *= n;
    }
    // Output stars
    for (int i = 1; i < prod; ++i) {
        std::cout << "*";
    }
    std::cout << "\n";
    return 0;
}
