import os
import glob
import config as config

fileList = glob.glob(config.out_dir+'/*')
 
# Iterate over the list of filepaths & remove each file.
def used(file):
  for f in config.exercises:
    if (f['slides']['link'] is not None) or (f['handout']['link'] is not None):
      if f['slides']['link'] == file or f['handout']['link'] == file:
        return True
  return False

for filePath in fileList:
  try:
    if not used(filePath):
      os.remove(filePath)
  except:
      print("Error while deleting file : ", filePath)