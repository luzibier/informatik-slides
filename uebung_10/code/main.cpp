#include <iostream>
#include <cassert>
#include "tribool.h"

void conjoin() {
  tribool result = tribool_true();
  tribool current = tribool_true();
  while (std::cin >> current) {
    result = result && current;
  }
  std::cout << result;
}

void test_constructors() {
  tribool_false();
  tribool_unknown();
  tribool_true();
}

void test_equality() {
  tribool tfalse = tribool_false();
  tribool tunknown = tribool_unknown();
  tribool ttrue = tribool_true();
  
  assert(tfalse == tfalse);
  assert(!(tfalse == tunknown));
  assert(!(tfalse == ttrue));
  
  assert(!(tunknown == tfalse));
  assert(tunknown == tunknown);
  assert(!(tunknown == ttrue));
  
  assert(!(ttrue == tfalse));
  assert(!(ttrue == tunknown));
  assert(ttrue == ttrue);
}

void test_and() {
  tribool tfalse = tribool_false();
  tribool tunknown = tribool_unknown();
  tribool ttrue = tribool_true();
  
  assert((tfalse && tfalse) == tfalse);
  assert((tfalse && tunknown) == tfalse);
  assert((tfalse && ttrue) == tfalse);
  assert((tunknown && tfalse) == tfalse);
  assert((tunknown && tunknown) == tunknown);
  assert((tunknown && ttrue) == tunknown);
  assert((ttrue && tfalse) == tfalse);
  assert((ttrue && tunknown) == tunknown);
  assert((ttrue && ttrue) == ttrue);
}

void test_or() {
  tribool tfalse = tribool_false();
  tribool tunknown = tribool_unknown();
  tribool ttrue = tribool_true();
  
  assert((tfalse || tfalse) == tfalse);
  assert((tfalse || tunknown) == tunknown);
  assert((tfalse || ttrue) == ttrue);
  assert((tunknown || tfalse) == tunknown);
  assert((tunknown || tunknown) == tunknown);
  assert((tunknown || ttrue) == ttrue);
  assert((ttrue || tfalse) == ttrue);
  assert((ttrue || tunknown) == ttrue);
  assert((ttrue || ttrue) == ttrue);
}

void test_bool_and() {
  tribool tfalse = tribool_false();
  tribool tunknown = tribool_unknown();
  tribool ttrue = tribool_true();
  
  assert((tfalse && true) == tfalse);
  assert((tfalse && false) == tfalse);
  assert((tunknown && true) == tunknown);
  assert((tunknown && false) == tfalse);
  assert((ttrue && true) == ttrue);
  assert((ttrue && false) == tfalse);
}

void test_bool_or() {
  tribool tfalse = tribool_false();
  tribool tunknown = tribool_unknown();
  tribool ttrue = tribool_true();
  
  assert((tfalse || true) == ttrue);
  assert((tfalse || false) == tfalse);
  assert((tunknown || true) == ttrue);
  assert((tunknown || false) == tunknown);
  assert((ttrue || true) == ttrue);
  assert((ttrue || false) == ttrue);
}

int main () {
  test_constructors();
  test_equality();
  test_and();
  test_or();
  test_bool_and();
  test_bool_or();
  conjoin();
}
