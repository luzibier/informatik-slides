% Chapter 1

% For referencing the chapter elsewhere, use \ref{Chapter1} 
%----------------------------------------------------------------------------------------

\section{Theorie}
Grundsätzlich sind Pointer nichts anderes als Referenzen. Anders als Referenzen, bei denen die referenzierte Variable ab der Initialisierung fix ist, können Pointer die referenzierten Variablen wechseln. Bisher habe wir bei der Traversierung von Listen immer den wahlfreien Zugriff (eckige Klammern oder \luziemphcode{.at()}) verwendet. Oft würde allerdings der sequentielle Zugriff reichen. D.h. zuerst Element 1, danach Element 2, etc. ...

Pointer speichern immer die Adresse im Arbeitsspeicher, an der das referenzierte Objekt gespeichert ist. Vor allem bei grossen  Objekten ist es sinnvoll, wenn immer möglich diese als Pointer zu übergeben. 

Wichtig zu wissen ist, dass unterschiedliche Objekte unterschiedlich viel Platz im Arbeitsspeicher benötigen. Dieser ist jeweils in Blöcke à 8-Bit aufgeteilt. Ein \luziemphcode{int} benötigt also 4 Blöcke, \luziemphcode{char} hat in einen Block Platz. Pointer zeigen immer auf den ersten Block der referenzierten Variable, und können anhand des Typs ausrechnen, wie mancher Block noch zum Objekt gehört.

\begin{figure}[hbtp!]
	\centering
	\includegraphics[width=0.4\linewidth]{Figures/Pointers}
	\caption{Pointers in \parencite{Wikipedia_Pointer}}
	\label{fig:pointers}
\end{figure}

\subsection{Pointer auf  eine Variable}
Ein Pointer ist immer vom selben Typ (Klasse) wie die Variable, die er referenziert. Zusätzlich folgt ein \luziemphcode{*}, um den Pointer zu kennzeichnen. 

\begin{lstlisting}
Typ var_name = wert;
Typ* ptr_name = &var_name; //ptr_name zeigt auf Adresse von var_name
\end{lstlisting}
Für Erklärungen zum \luziemphcode{\&} siehe Referenzierungsoperator (Abschnitt \ref{deref})

\subsubsection{Referenzierung und Dereferenzierung} \label{deref}
Da im Pointer selbst nur die Adresse der Variable gespeichert ist, wir aber oft an dessen Wert interessiert sind, müssen wir wieder das Objekt dahinter erhalten. Wir müssen es dereferenzieren. Dazu verwenden wir den Dereferenzierungsoperator \luziemphcode{*}. Schreiben wir ein \luziemphcode{*} vor einen Pointer, erhalten wir das Objekt hinter dem Pointer. 

Umgekehrt können wir eine Variable haben, aber dessen Adresse benötigen. Dafür brauchen wir den Referenzierungsoperator \luziemphcode{\&}. Schreiben wir diesen vor einen Variablennamen, erhalten wir die Adresse der Variable im Arbeitsspeicher. 

\paragraph{Allgemeiner Aufbau}
\begin{lstlisting}
Typ var_name = wert;
Typ* ptr_name = &var_name; // Referenzierung
Typ var_name_2 = *ptr_name; // Dereferenzierung
var_name2 == var_name; // true;\end{lstlisting}

\begin{block}{Implizite Dereferenzierung}
Die Referenzierung kann z.T. auch ohne explizites Schreiben des Referenzierungsoperators geschehen. Beispiel: int a [] = {1, 2, 3}; int* b = a;
\end{block}

\ifexamples
\paragraph{Beispiel}
\begin{lstlisting}
struct Complex {
	double re, im;
}

int main(){
	Complex c1 {1, 2};
	Complex* ptr1 = &c1; // Referenzierung
	Complex c2 = *prt1; // Dereferenzierung
}\end{lstlisting}
\fi

\subsubsection{new}
Der Operator \luziemphcode{new} gibt uns einen Pointer auf eine Instanz eines Objektes zurück, die irgendwo im Arbeitsspeicher für uns angelegt wurde. Besonders wichtig wird das in Kapitel \ref{Chapter12}, Dynamic Data Structures.

\paragraph{Allgemeiner Aufbau}
\begin{lstlisting}
Typ* ptr_name = new Typ(...)\end{lstlisting}
\paragraph{Beispiel}
\begin{lstlisting}
struct Complex {
	double re, im;
}

int main(){
	Complex* prt_1 = new Complex{1, 2};
}\end{lstlisting}

\subsection{Null-Pointer}
Der Null-Pointer \luziemphcode{nullptr} funktioniert wie ein Platzhalter. Wir können jeden Pointer gleich \luziemphcode{nullptr} setzen.

\subsection{Pointer auf ein Array}
\begin{alertblock}{Achtung}
Wir sprechen hier nur von Arrays! Für Vektoren und weitere Container können wir keine Pointer verwenden, sondern brauchen Iteratoren. Diese findet ihr in Kapitel \ref{Chapter11}.
\end{alertblock}
Da wir Pointer auf alle Objekte machen können, existieren auch Pointer auf Arrays. Da Arrays zusammenhängende Bereiche im Arbeitsspeicher sind, sind Pointer auf Arrays besonders nützlich. Der Typ der Pointers bleibt derjenige des Arrays. Referenzieren wir nicht explizit ein anderes Element, zeigt der Pointer auf das erste Element.

\paragraph{Allgemeiner Aufbau}
\begin{lstlisting}
Typ arr_name [nr_of_elems] ;
Typ* ptr_1 = &arr_name; 
Typ* ptr_2 = &arr_name[0]; 
Typ* ptr_3 = arr_name; // ptr_1 == ptr_2 == ptr_3
Typ* ptr_4 = arr_name[4]; // Zeigt auf 5tes Element\end{lstlisting}

\begin{alertblock}{Achtung}
Pointer speichern keine Längeninformation. Haben wir nur einen Pointer auf ein Array, wissen wir nicht, wann es zu Ende ist.
\end{alertblock}

\ifexamples
\subsubsection{Beispiel}
\begin{lstlisting}
int [] arr = {1, 2, 3, 4, 5}; 
int* arr_start = &arr; // Pointer auf Beginn des  Arrays \end{lstlisting}
\fi

\subsubsection{new}
Wie auch mit Variablen, erhalten wir bei Arrays mit \luziemphcode{new} einen Pointer auf eine Liste irgendwo im Arbeitsspeicher. 

\begin{lstlisting}
Typ* pointer =  new Typ [no_of_elems] {_, _, _, ...}; \end{lstlisting}

\ifexamples
\paragraph{Beispiel}
\begin{lstlisting}
int* pointer =  new int [6] {1, 2, 3, 4, 5}; // Array der Laenge 6 mit 5 initialisierten Elementen\end{lstlisting}
\fi

\subsubsection{Pointer-Arithmetik}
Haben wir zusammenhängende Speicherbereiche (z.B. bei Arrays) können wir unsere Pointer \luziemphcode{+} und \luziemphcode{-} ein \luziemphcode{int} rechnen um von einem Element zum nächsten zu springen. Natürlich funktionieren die Operatoren \luziemphcode{++ - - += -=} auch. Auch können wir Pointer auf Gleichheit prüfen und ihre Grössen vergleichen. Der grössere Pointer ist jeweils der, der eine höhere Adresse enthält, d.h. auf ein Element weiter hinten im Array zeigt. 

\paragraph{Allgemeiner Aufbau}
\begin{lstlisting}
Typ* pointer =  new Typ [no_of_elems] {_, _, _, ...}; // hier MUESSEN wir die Laenge des Arrays angeben.
pointer += 3;
std::cout << *pointer; // Ausgabe von Element 4;\end{lstlisting}

\begin{alertblock}{Vorsicht!}
Pointer-Arithmetik funktioniert nur, wenn wir einen \textbf{zusammenhängenden Speicherbereich} (beispielsweise ein Array) haben. Auch müssen wir selbst überprüfen, dass wir mit den Pointern innerhalb des Bereichs bleiben.
\end{alertblock}

\ifexamples
\paragraph{Beispiel}
\begin{lstlisting}
int* pointer = new int [5] {1, 2, 3, 4, 5}; 
int* ptr = pointer;
ptr += 4; // Zeigt auf letztes Element
std::cout << ptr--; // Ausgabe: 5  // Vorsicht mit Pre und Postfix
std::cout << --ptr; // Ausgabe: 3
std::cout << pointer <= ptr; // Ausgabe: true\end{lstlisting}
\fi

\subsubsection{Traversierung von Arrays mit Pointern}
Nebst wahlfreiem Zugriff können wir auch mit sequenziellem Zugriff durch ein Array traversieren. Da der sequenzielle Zugriff viel schneller ist, ist nach Möglichkeit immer der sequentielle Ansatz zu verwenden.

\paragraph{Allgemeiner Aufbau} Oft wird ein For-Loop verwendet.
\begin{lstlisting}
Typ* ptr_begin =  Typ [length] {_, _, ...}; 
Typ* ptr_end =  &ptr_begin[length]; // Past-the-end pointer

for(Typ ptr = ptr_begin; ptr < ptr_end; ptr++){
	std::cout << *ptr;
}\end{lstlisting}

\ifexamples
\paragraph{Beispiel}
\begin{lstlisting}
#include <iostream>

int main(){
	int* ptr_begin =  int [6] {1, 2, 3, 4, 5, 6}; 
	int* ptr_end =  &ptr_begin[6]; // Past-the-end pointer
	
	for(Typ ptr = ptr_begin; ptr < ptr_end; ptr++){
		std::cout << *ptr << ", "; // Ausgabe: 1, 2, 3, 4, 5, 6, 
	}
}\end{lstlisting}
\fi

\subsubsection{Array mit Pointern übergeben}
Da Pointer keine Längeninformation enthalten, müssen wir, wenn wir ein Array mit Pointern einer Funktion übergeben wollen, eine andere Lösung finden. Entweder gibt man als weiteres Funktionsargument die Länge des Arrays mit, oder - viel besser - den sogenannten Past-the-end-Pointer. Dieser zeigt hinter das letzte Element des Arrays. Haben wir in unserer Funktion den Past-the-End-Pointer erreicht, wissen wir, dass wir am Ende des Arrays angelangt sind.

\paragraph{Allgemeiner Aufbau}
\begin{lstlisting}
ret_value function_name (Typ* begin_ptr_name, Typ* end_ptr_name){
	// Do stuff
}

int main(){
	Typ* ptr_begin =  Typ [length] {_, _, ...}; 
	Typ* ptr_end =  &ptr_begin[length]; // Past-the-end pointer
	
	function_name(ptr_begin, ptr_end);
}\end{lstlisting}

\ifexamples
\paragraph{Beispiel}
\begin{lstlisting}
#include <iostream>

int getSum(int* begin, int* end){
	int sum = 0;
	for (int* ptr = begin; ptr < end; ptr++){
		sum += *ptr;
	}
}

int main(){
	int* array =  Typ [8] {1, 2, 1, 2, 1, 2, 1, 2}; 
	const int* array_end =  &array[8]; // Past-the-end pointer
	
	std::cout getSum(array, array_end); // Ausgabe: 12
}\end{lstlisting}
\fi

\subsection{this-Pointer}
Sind wir in einer Klasse drin, erhalten wir mit dem Ausdruck \luziemphcode{this} einen Pointer auf die eigene Instanz des Objektes. Dies findet vor allem Anwendung zur besseren Lesbarkeit, wenn wir Memberfunktionen haben, die verschiedene Instanzen derselben Klasse verarbeiten. \luziemphcode{this} ist unerlässlich, wenn wir in einer Funktion ein Argument haben, das gleich heisst, wie eine Membervariable. Schreiben wir kein \luziemphcode{this}, wäre sonst immer das Argument gemeint.

\ifexamples
\subsubsection{Beispiel}
Wieder zurück auf Vec2D:

\begin{lstlisting}
class Vec2D{
	double x, y;
	
	public:
	Vec2D(double x, double y){
		(*this).x = x;
		(*this).y = y;
	}
}
\end{lstlisting}
\fi

\subsection{const und Pointer}
\luziemphcode{const} kann bei Pointern zwei Bedeutungen haben: 
\begin{itemize}
	\item Wir können das referenzierte Objekt (nach der Initialisierung des Pointers) nicht mehr ändern.
	\item Wir können den Wert des referenzierten Objektes nicht ändern.
\end{itemize}

\paragraph{Allgemeiner Aufbau}
\begin{lstlisting}
Typ var_name = wert;
Typ* ptr = &var_name; // Normaler Pointer
Typ const* ptr = &var_name; // Pointer auf konstantes Objekt, koennen keinen neuen Wert setzen
Typ* const ptr = &var_name; // Konstanter Pointer, kann die Referenz nicht aendern
Typ const* const ptr = &var_name; // Konstanter Pointer auf konstantes Objekt \end{lstlisting}



\begin{alertblock}{Achtung}
Ist eine referenzierte Variable \luziemphcode{const}, dann muss der Pointer auch auf ein konstantes Objekt zeigen! (\luziemphcode{Typ const* ptr = \&var\_name;})
\end{alertblock}

\ifexamples
\paragraph{Beispiel}
\begin{lstlisting}
int a = 20;
const int b = 10;
int c = 5;

int* ptr1 = a; // OK
int* ptr2 = b; // Fehler, b ist read-only

int const* ptr3 = a; // OK
int const* ptr4 = b; // OK

int* const ptr5 = a; // OK
int* const ptr6 = b; // Fehler, b ist read-only
ptr5 = &c; // Fehler, ptr5 ist konstant

int const* const ptr7 = a; // OK
intconst* const ptr8 = b; // OK
ptr7 = &c; // Fehler, ptr7 ist konstant\end{lstlisting}
\fi

\subsection{Tipps Pointern}\label{ptrtipps}
Pointer sind recht tricky in der Handhabung, deshalb hier noch ein paar Tipps.
\subsubsection{Subtraktion}
Subtrahieren wir zwei Pointer von einander, erhalten wir die Anzahl Elemente zwischen den Pointern + 1.

\ifexamples
\paragraph{Beispiel}
\begin{lstlisting}
#include <iostream>
int main(){
	int* ptr = new int[8]{1, 2, 3, 4, 5};
	int* ptr2 = &ptr[3];
	
	std::cout << ptr2-ptr; // Ausgabe: 3
}\end{lstlisting}
\fi

\subsubsection{->}
Haben wir eine Klasse mit \luziemphcode{public} Memberfunktionen oder -variablen referenziert (in einem Pointer gespeichert), und wollen auf einen Member zugreifen, müssen wir zuerst den Pointer dereferenzieren. Anschliessend können wir auf die Member mit einem \luziemphcode{.} zugreifen. 

\ifexamples
\paragraph{Beispiel}
\begin{lstlisting}
#include <iostream>

class Vec2D{
	double x, y;
	
	public:
	Vec2D(double x, double y){
		(*this).x = x;
		(*this).y = y;
	}
	
	double getX(){
		return x;
	}
	
	double getY(){
		return y;
	}
}

int main(){
	Vec2D* v = new Vec2D(1, 2);
	
	std::cout << (*v).getX() << ", " << (*v).getY() << std::endl; 
}\end{lstlisting}
\fi

\begin{block}{Kurzform}
Anstelle von \luziemphcode{(*var\_name).var\_name2 } können wir auch \luziemphcode{var\_name->var\_name2} schreiben, dies ist komplett gleichbedeutend. Das gilt natürlich auch für Funktionen.
\ifexamples
\begin{lstlisting}
#include <iostream>

class Vec2D{
	public:
		double x, y;
	
		Vec2D(double x, double y){
			this->x = x;
			this->y = y;
		}
		
		double getX(){
			return x;
		}
}

int main(){
	Vec2D* v = new Vec2D(1, 2);
	
	std::cout << v->x << ", " << v->y << std::endl; 
	std::cout << v->getX();
}\end{lstlisting}
\fi
\end{block}

\subsubsection{[]}
Addieren wir eine Zahl zu einem Pointer und geben den Wert hinter dem Pointer aus, müssen wir in anschliessend dereferenzieren.

\ifexamples
\paragraph{Beispiel}
\begin{lstlisting}
#include <iostream>

int main(){
	int* ptr = new int[8]{1, 2, 3, 4, 5};
	std::cout << *(ptr+2); // Ausgabe: 3
}\end{lstlisting} 
\fi

\begin{block}{Kurzform}
	Wie im Abschnitt oben, gibt es auch eine Kurzform für \luziemphcode{*(ptr+value)}, diese lautet \luziemphcode{ptr[value]}.
	
\ifexamples
\begin{lstlisting}
#include <iostream>

int main(){
	int* ptr = new int[8]{1, 2, 3, 4, 5};
	std::cout << ptr[2]; // Ausgabe: 3
}\end{lstlisting}
\fi	
	
\end{block}
