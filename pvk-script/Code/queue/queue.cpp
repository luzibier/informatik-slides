#include <iostream>
#include "queue.h"

// PRE: 2D-Vektor wird so eingegeben: (x_koord, y_koord), bsp: (1.2, 4.5)
// POST: Speichert den Vekotor in der Referenz.
std::istream& operator>>(std::istream& in, Vec2D& v){
  char c;
  in >> c; // loesche "("
  in >> v.x; // speichere x_koord
  in >> c >> c; // loesche ", "
  in >> v.y; // speichere y_koord
  in >> c; // loesche ")"
  return in;
}

// PRE: Gueltiger Vec2D
// POST: Gibt den Vektor in der obigen Form aus.
std::ostream& operator<<(std::ostream& out, Vec2D v){
  return out << '(' << v.x << ", " << v.y << ')'; // ausgeben eines Vec2D
}

// PRE: Gueltige Queue
std::ostream& operator<<(std::ostream& out, Queue q){
  q.print(out); // rufe Print-Funktion von Queue
  return out;
}

// loesche Nodes ab Start
void Queue::deleteNodes(Node* start){
  if (start != nullptr){
    deleteNodes(start->next); // loesche alle Nodes nach start
  }
  delete start; // wenn alle nach start geloscht sind, losches start auch
}

// Gibt alle Nodes ab startan out aus.
void Queue::printNodes(Node* start, std::ostream& out){
  if (start == nullptr){ 
    return; // Beginne mit return, wenn am Ende der Queue
  }
  out << start->value << ((start->next!=nullptr) ? ", " : ""); // Gib den Wert aus --> << wurde fuer Vec ueberladen
  printNodes(start->next, out); // Rekursionsschritt
}

// Default Konstruktor
// s : Name
Queue::Queue(std::string name){
  first = nullptr;
  last = nullptr;
  size = 0;
  this->name = name; // setze meinen Namen auf name
}

// Destruktor
// loescht alle dynamisch allozierten Werte
Queue::~Queue(){
  deleteNodes(first); // loesche Nodes vom ersten an
}

// kopiert alle dynamisch allozierten Werte
Queue::Queue(const Queue& other){
  if(other.first == nullptr){ // falls other leer
    first = nullptr;
    last = nullptr;
  } else{ 
    first = new Node{other.first->value, nullptr}; // first muss speziell gesetzt werden
    Node* current_other = other.first->next; // Vorbereiten fuer Loop
    Node* current_this = first;
    
    while(current_other != nullptr){
      Node* temp = new Node{current_other->value, nullptr}; // Zwischenzeitlicher Name
      current_this->next = temp;
      
      current_other = current_other->next; // Traversieren der Queue
      current_this = temp;
    }
    last = current_this; // last muss speziell gesetzt werden
  }
  name = "(Kopie von " + other.name + ")";
  size = other.size;
}

// weis dieser Queue die Queue other zu
Queue& Queue::operator=(const Queue& other){
  name = "(gleiche Queue wie " + other.name + ")";
  size = other.size;
  
  deleteNodes(first); // Loesche meine Nodes
  
  first = other.first; // Setzte auf die Nodes von other
  last = other.last;
  
  return *this; // muss eine Referenz zurueck geben
}

// POST: Gibt die Groesse der Liste zurueck
unsigned int Queue::getSize(){
  return size; // auch return this->size moeglich
}

// POST: Setzt den Namen der Queue
std::string Queue::getName(){
  return name;
}

// PRE: valid ostream
// POST: prints the Queue to the stream
void Queue::print(std::ostream& out){
  out << "Queue heisst \"" << name << "\" und enthaelt " << size << " Element" << ((size != 1) ? ("e") : ("")) << std::endl;
  out << "Inhalt: ";
  printNodes(first, out); // Rufe Print-Funktion ab erstem Node auf
  out << std::endl;
}

// PRE: gueltiger Vec2D
// POST: Fuege Element zu Queue hinzu (neuer Last Node)
void Queue::enqueue(Vec2D input){
  Node* new_last = new Node{input, nullptr}; // brauchen kurz last und den neuen Node, --> zukuenftiges last
  if (last == nullptr){
    first = new_last; // falls erstes Element in der Queue
  } else{
    last->next = new_last; // Setzt next von aktuellem last uf zukuenftiges last
  }
  last = new_last; //Setzte last auf zukuenftiges last
  size++;
}

// PRE: Queue nicht leer
// POST Loesche letztes Element aus der Queue und gib dessen Wert zurueck.
Vec2D Queue::dequeue(){
  if (first == nullptr){ // Queue ist leer
    std::cerr << "Queue ist leer";
    return {0, 0};
  }
  
  Node* new_first = first->next; // zukuenftiger first, brauchen fuer den Moment beide
  Vec2D ret = first->value; // speichere Resultat
  
  delete first; // loesche den aktuellen first, haben ja den value gespeichert
  first = new_first; // setze first auf zukuenftigen first
  
  size--;
  return ret;
}

// PRE: Gueltige Queue
// POST: Setzt zwei Queues zusammen. Haengt Kopie der Werte an und gibt neue Queue zurueck
Queue Queue::operator+(const Queue& other){
  Queue copy_this = *this; // Verwenden den Copy-Konstruktor.
  Queue copy_other = other; // Copy-Konstruktor
  
  while(copy_other.first != nullptr){ // solange noch Elemente in copy_other...
    copy_this.enqueue(copy_other.dequeue()); // ...dequeue Element aus copy_other und lade sie mit enqueue wieder in copy_this 
  }
  
  copy_this.name = "(Merge von " + this->name + " und " + other.name + ")";
  
  return copy_this;
}


// PRE: Gueltige Queue
// POST: Haengt Kopie der Werte von other an diese Queue.
Queue& Queue::operator+=(const Queue& other){
  Queue copy_other = other; // Copy-Konstruktor
  
  while(copy_other.first != nullptr){// solange noch Elemente in copy_other...
    this->enqueue(copy_other.dequeue()); // dequeue Element aus copy_other und enqueue es in diese Queue
  }
  
  name += " erweitert um " + other.name;
  return *this; 
}


