**MOVED!** *Since my Studies are ending soon, the project has been moved to gitlab.com ([here](https://gitlab.com/lujobi-projects/studies/assistance/informatics-1/slides)).*


Informatik Übung
================

Only `.tex` files all builds will automatically be published to [gitlab.ethz.ch/luzibier/info-1](https://gitlab.ethz.ch/luzibier/info-1)!
